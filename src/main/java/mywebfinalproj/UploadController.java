package mywebfinalproj;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


@RestController
@RequestMapping(value = "/file")
public class UploadController {

    private static String upload_dir = "/home/user/Documents/randomdir";

    @Autowired
    private
    UploadRepo fileRepo;

    @RequestMapping(headers=("content-type=multipart/*"),value = "", method = RequestMethod.PUT)
    public ResponseEntity imageUpload(@RequestParam("filename") MultipartFile file) throws Exception {
        try {

            String extension = getExtensionOfFile(file.getOriginalFilename());
            if(!extension.equals("txt")) throw new Exception("No .txt format");

            byte[] bytes = file.getBytes();

            UploadFile newFile = new UploadFile();
            String pathToFile = "";

            String contentOfFile = "";
            InputStream inputStream = file.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                contentOfFile = contentOfFile + line;
            }

            String filename = UUID.randomUUID().toString();

            if(contentOfFile.contains("spam")) {
                File fileInLocal = new File(upload_dir + "spamFiles");
                if(!fileInLocal.exists()) {
                    fileInLocal.mkdir();
                }
                pathToFile = upload_dir + "spamFiles/" +  filename + ".txt";
                newFile.setFileName(file.getOriginalFilename());
                newFile.setFilePath(pathToFile);
                newFile.setType("SPAM");
                System.out.println(filename);
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();
                String time = sdf.format(date);

                File fileInLocal = new File(upload_dir + time);
                if(!fileInLocal.exists()) {
                    fileInLocal.mkdir();
                }
                pathToFile = upload_dir + time + "/" + filename + ".txt";
                newFile.setFileName(file.getOriginalFilename());
                newFile.setFilePath(pathToFile);
                newFile.setType("NOTSPAM");
            }

            Path path = Paths.get(pathToFile);
            Files.write(path, bytes);
            newFile.setBytes(bytes);
            fileRepo.save(newFile);

        } catch (IOException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @GetMapping(path="/dateFiles") // Map ONLY GET Requests
    public @ResponseBody Iterable<UploadFile> showDateFiles() throws Exception {
        return fileRepo.findByType("NOTSPAM");
    }

    @GetMapping(path="/spamFiles") // Map ONLY GET Requests
    public @ResponseBody Iterable<UploadFile> showSpamFiles() throws Exception {
        return fileRepo.findByType("SPAM");
    }

    public String getExtensionOfFile(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i >= 0) {
            extension = fileName.substring(i+1);
        }
        return extension;
    }
}
